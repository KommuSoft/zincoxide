Cleaning code
=============

Tasks
-----

Cleaning code implies:

 - cleaning source code such that it is less error prone `Source`,
 - writing checks on the input parameters `ParCheck`,
 - write contracts for pre- and post-conditions `Contract`,
 - documenting the source code `Specs`,
 - writing relevant tests `Tests`, and
 - performing static checks with `cccheck`, `StaticCh`.

Namespace log
-------------

The following table shows the logbook of code cleaning for the ZincOxide project.

| Namespace | Source | ParCheck | Contract | Specs | Tests | StaticCh |
|:---|---|---|---|---|---|---|
| `Codegen` | | | | 20140908 | |
| `Codegen.Abstract` | | | | 20140908 | |
| `Codegen.Abstract.Imperative` | 20140907 | 20140907 | 20140908 | 20140908 | |
| `Codegen.Abstract.OO` | | | 20140908 | 20140908 | |
| `Codegen.Abstract.OO.CSharp` | | | | 20140908 |
| `Codegen.Abstract.OO.Process` | | | | 20140908 | |
| `Codegen.Abstract.Results` | | | | 20140908 | |
| `Codegen.Abstract.Typed` | | | | 20140908 | |
| `Codegen.Abstract.Concrete` | | | | 20140908 | |
| `Codegen.Abstract.Concrete.OO` | | | | 20140908 | |