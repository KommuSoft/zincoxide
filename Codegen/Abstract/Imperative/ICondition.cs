//
//  ICondition.cs
//
//  Author:
//       Willem Van Onsem <vanonsem.willem@gmail.com>
//
//  Copyright (c) 2014 Willem Van Onsem
//
//  This program is free software: you can redistribute it and/or modify
//  it under the terms of the GNU General Public License as published by
//  the Free Software Foundation, either version 3 of the License, or
//  (at your option) any later version.
//
//  This program is distributed in the hope that it will be useful,
//  but WITHOUT ANY WARRANTY; without even the implied warranty of
//  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
//  GNU General Public License for more details.
//
//  You should have received a copy of the GNU General Public License
//  along with this program.  If not, see <http://www.gnu.org/licenses/>.
using System;
using System.Diagnostics.Contracts;

namespace ZincOxide.Codegen.Abstract.Imperative {

	/// <summary>
	/// An interface specifying a condition: an expression that results in a boolean. Such conditions can be used
	/// for <c>if</c> statements and <c>for</c> and <c>while</c> loops.
	/// </summary>
	/// <remarks>
	/// <para>A condition is an expression that returns a boolean value.</para>
	/// </remarks>
	[ContractClass(typeof(ConditionContract))]
	public interface ICondition : IExpression {
	}
}